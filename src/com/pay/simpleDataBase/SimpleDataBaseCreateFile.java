package com.pay.simpleDataBase;

import com.pay.model.Client;


import java.io.*;
import java.util.List;
import java.util.Set;

/*
Zapisujemy dane użytkowników do plików .txt
 */
public class SimpleDataBaseCreateFile implements Serializable {

//C:\Users\User\IdeaProjects2\BankApp\src\com\pay\simpleDataBase

//C:\Users\User\IdeaProjects2\BankApp\src\com\pay\simpleDataBase

    public void createTextFile(List<Client> clientList) {
        String FILENAME = clientList.get(0).getAccountNumber() + ".txt";
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {

            String content =
                    clientList.get(0).getFirstName() + " "
                    + clientList.get(0).getLastName() + " "
                    + clientList.get(0).getAccountNumber() + " "
                    + clientList.get(0).getAccountBalance() + " "
                    + clientList.get(0).getAccountPassword() + " "
                    + clientList.get(0).getAccountLogin() + " "
                    + clientList.get(0).getCity() + " "
                    + clientList.get(0).getStreet() + " "
                    + clientList.get(0).getNumberOfHouse() + " "
                    + clientList.get(0).getPostalCode();

            fw = new FileWriter(FILENAME);
            bw = new BufferedWriter(fw);
            bw.write(content);

            System.out.println("Udało się zapisać Twoje dane w systemie!!!\n");

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null)
                    bw.close();

                if (fw != null)
                    fw.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

    }


}
/*
       public static void writeToTXTFile (String file)
                                        throws FileNotFoundException {
        PrintWriter out = new PrintWriter(file);
        out.printf("%-4s %-20s %-20s %-4s", "Id", "Imię", "Nazwisko", "Wiek\n");
        for (ListController lf: listToFile)
            out.printf("%-4d %-20s %-20s %-4d\n",
                    lf.getPersonId(), lf.getPersonFirstName(),
                    lf.getPersonLastName(), lf.getPersonAge());
        out.close();
    }


    */