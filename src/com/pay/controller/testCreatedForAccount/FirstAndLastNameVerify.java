package com.pay.controller.testCreatedForAccount;

/*
Sprawdzamy czy nie ma spacji w dwuczłonowym imieniu lub nazwisku
 */
public class FirstAndLastNameVerify implements AccountVerify {

    public boolean isAdequate(String Name) {
        int first = Name.trim().length();

        String ReplacedName = Name.replaceAll("[^A-Za-z]+", "");

        int second = ReplacedName.length();

        if(first != second){
         return false;
        }
        else return true;
    }

}
