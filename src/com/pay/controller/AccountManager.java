package com.pay.controller;

import com.pay.controller.testCreatedForAccount.AccountVerify;
import com.pay.controller.testCreatedForAccount.FirstAndLastNameVerify;
import com.pay.controller.testCreatedForAccount.PostalCodeVerify;
import com.pay.model.Client;
import com.pay.simpleDataBase.SimpleDataBaseCreateFile;

import java.io.FileNotFoundException;
import java.util.*;

/*
Nadajemy wartości polom z danymi klienta
 */
public class AccountManager {

    private static List<Client> listOfClient = new ArrayList<>();
    SimpleDataBaseCreateFile simpleDataBaseCreateFile = new SimpleDataBaseCreateFile();
    Client client = new Client();

    public void addNewClient() {
        AccountVerify accountVerify = new FirstAndLastNameVerify();

        boolean optionFlag2 = true;

        System.out.println(
                "<--------------------------------------->" +
                        "\n\tWitaj w systemie tworzenia konta!!!" +
                        "\n\tPodaj swoje dane według poleceń:\n" +
                        "<--------------------------------------->\n");
        try {
            Scanner scanner = new Scanner(System.in);
            Scanner input = new Scanner(System.in);
            while (optionFlag2) {
                Long accountBalance = 0L;
                client.setAccountBalance(accountBalance);

                CreatingAccountNumber creatingAccountNumber = new CreatingAccountNumber();
                client.setAccountNumber(creatingAccountNumber.getAccountNumber());

                System.out.println("Podaj swoje imie (Jeśli imie jest dwuczłonowe użyj myślnika):");
                String firstName = input.nextLine();
                if (accountVerify.isAdequate(firstName))
                    client.setFirstName(firstName.trim());
                else break;

                System.out.println("Podaj swoje nazwisko (Jeśli imie jest dwuczłonowe użyj myślnika):");
                String lastName = input.nextLine();
                if (accountVerify.isAdequate(lastName.trim()))
                    client.setLastName(lastName);
                else break;


                System.out.println("Podaj swój adres\nPodaj miasto:");
                client.setCity(input.nextLine());

                System.out.println("Podaj ulicę:");
                client.setStreet(input.nextLine());

                System.out.println("Podaj kod pocztowy w formacie AB-CDE:");
                String postalCode = input.nextLine();

                accountVerify = new PostalCodeVerify();
                if (accountVerify.isAdequate(postalCode.trim()))
                    client.setPostalCode(postalCode);
                else
                    break;

                System.out.println("Podaj numer domu:");
                client.setNumberOfHouse(scanner.nextInt());

                System.out.println("Podaj swój login:");
                client.setAccountLogin(input.nextLine());

                System.out.println("Twój login to: " + client.getAccountLogin());

                System.out.println("Podaj swóje hasło:");
                client.setAccountPassword(input.nextLine());


                getListOfClient().add(client);
                instantCreatFile();

                System.out.println(
                        "<------------------------------------->" +
                                "\nWitaj w systemie bankowym użytkowniku:\n" +
                                client.getFirstName() + " " + client.getLastName() +
                                "\nTwój numer konta to: " + client.getAccountNumber() +
                                "\n<------------------------------------->" +
                                "\n\tTwój obecny stan konta równa się 0.00 zł." +
                                "\n\tProponujemy dokonac wpłaty na konto.\n" +
                                "<------------------------------------->\n\n\n");
                getListOfClient().remove(0); //// usuwamy wszystkie obiekty z listy korzystamy z plików .txt
                optionFlag2 = false;
            }
        } catch (InputMismatchException e) {
            System.out.println("Wprowadzono niepoprawny format danych!!!\nPonów próbę!!!");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void instantCreatFile() throws FileNotFoundException {
        simpleDataBaseCreateFile.createTextFile(getListOfClient());

    }


    public List<Client> getListOfClient() {
        return listOfClient;
    }


}
