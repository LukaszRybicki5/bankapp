package com.pay;

import com.pay.controller.AccountManager;
import com.pay.model.Client;

import java.io.IOException;
import java.util.Scanner;

import static java.lang.System.exit;


public class BankApp {
    public static void main(String[] args) throws IOException {

        Scanner input = new Scanner(System.in);
        AccountManager accountManager = new AccountManager();
        Client client = new Client();

        boolean optionFlag = true;

        while (optionFlag) {
            System.out.println("<--------------------------------->" +
                    "\n\tWitaj w systemie bankowym!!!" +
                    "\n<---------------------------------> " +
                    "\nWybierz co chcesz zrobić:" +
                    "\na) Założenie konta" +
                    "\nb) Logowanie do systemu" +
                    "\nx) Zamknięcie programu");

            String choice = input.nextLine();

            switch (choice) {
                case "a":  //zakładamy nowe konto, nadajemy wartości polom z package model
                    accountManager.addNewClient();
                    break;

                case "b":
                    break;
                case "x":
                    exit(0);
                    break;
                default:
                    System.out.println("Błąd!!! \nWybierz opcje " +
                            "(wpisz małą literę a lub b w konsolę).");
                    break;

            }
        }


    }
}
